import  ApolloClient, { InMemoryCache, NormalizedCacheObject  }  from 'apollo-boost';
import withApollo, { InitApolloOptions } from 'next-with-apollo';


// Update the GraphQL endpoint to any instance of GraphQL that you like
const GRAPHQL_URL: string = 'https://booster.games/api/';

// Export a HOC from next-with-apollo
// Docs: https://www.npmjs.com/package/next-with-apollo
export default withApollo(({ initialState } : InitApolloOptions<NormalizedCacheObject>) =>
        new ApolloClient({
            uri: GRAPHQL_URL,
            fetchOptions: {
                credentials: 'include'
            },
            cache: new InMemoryCache().restore(initialState || {})
        })
);