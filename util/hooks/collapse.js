import React from 'react'
import {Collapse} from "antd";

const {Panel} = Collapse;

const ImageArrow = (e) => {

    const activeStyle = {
        transform: "rotate(0deg)"
    }

    const style = {
        transform: "rotate(180deg)"
    }

    const stylesImg = e.e.isActive? activeStyle: style;

    return (
        <img src="/arrow.png" alt="arrow" style={stylesImg} className="imageArrow"/>
    )
}

export const CollapseFaq = (props) => {
    return(
        <Collapse accordion
                  expandIconPosition={'right'}
                  className="site-collapse-custom-collapse"
                  expandIcon={(e) => <ImageArrow e={e}/>}
                  defaultActiveKey={false} {...props}>
        </Collapse>
    )
}

export const PanelFaq = (props) => {
    return(
        <Panel {...props}>

        </Panel>
    )
}
