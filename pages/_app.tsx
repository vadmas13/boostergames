import * as React from 'react';
import App, { AppProps } from 'next/app';
import {ApolloProvider} from '@apollo/react-hooks';
import { ApolloClient } from "apollo-boost";
import withData from '../util/apollo-client';
import 'antd/dist/antd.css';
import './../assets/styles/fonts.scss'
import './../assets/styles/CollapseFaq.css'
import './../assets/styles/MenuAntd.css'
import './../assets/styles/sectionsAntd.css'

interface Props {
    apollo: ApolloClient<{}>;
}

class MyApp extends App<Props, AppProps>{

    render() {
        const {Component, pageProps, apollo} = this.props;
        return (
            <ApolloProvider client={apollo}>
                <Component {...pageProps} />
                <style global jsx>{`
                      html,
                      body,
                      body > div:first-child,
                      div#__next,
                      div#__next > div,
                      main,
                      div#__next > div > div {
                        height: 100%;
                      }
                `}</style>
            </ApolloProvider>
        );
    }
}


// Wraps all components in the tree with the data provider
export default withData(MyApp);