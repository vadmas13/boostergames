import  {useEffect, useState, FC} from 'react'
import React from 'react'
import Head from 'next/head'
import {useQuery} from "@apollo/react-hooks";
import {
    FETCH_CATEGORIES_PARENTS_QUERY,
    FETCH_FAQ_OF_CATEGORIES,
    FETCH_FAQ_OF_GAME,
    FETCH_FAQ_OFFERS
} from "../graphql/query";
import MenuBar from "../components/MenuBar";
import { Layout } from 'antd';
import gridStyles from './../assets/styles/grid_styles.module.scss'
import ContentGame from "../components/ContentGame";
import LoadingPage from "../components/LoadingPage";
import allCategoriesType, {ParentType} from './../types/allCategories'
import DataTypes from "../types/category";
import {ResultDataType} from "../types/common";

const {Sider, Content } = Layout;


const Home: FC = () => {

    const [categories, setCategories] = useState<Array<ParentType> | undefined>([]);
    const [activeGame, setActiveGame] = useState<string>();

    const {data, loading} = useQuery<allCategoriesType>(FETCH_CATEGORIES_PARENTS_QUERY);
    const fetchGameFaq  = useQuery<DataTypes, ResultDataType>(FETCH_FAQ_OF_GAME, {
        variables: {
            id: activeGame
        }
    })

    const fetchCategoriesFaq = useQuery<DataTypes, ResultDataType>(FETCH_FAQ_OF_CATEGORIES, {
        variables: {
            id: activeGame
        }
    })

    const fetchOffersFaq = useQuery<DataTypes, ResultDataType>(FETCH_FAQ_OFFERS, {
        variables: {
            id: activeGame
        }
    })

    useEffect(() => {
        if (data) {
            let parentItems = data.allCategories.edges.filter(req => req.node.parent === null);
            let parents = parentItems.map(req => {
                return {id: req.node.id, name: req.node.name}
            });
            setActiveGame(parents[0].id.toString())

            setCategories(parents);
        }
    }, [data])



    if (loading) {
        return <LoadingPage />
    } else {
        return (
            <div className="container">
                <Head>
                    <title>Booster games</title>
                    <link rel="icon" href="/favicon.ico"/>
                </Head>
                <main>
                    <Layout className={gridStyles.gridStyles}>
                        <Sider className={gridStyles.gridStyles__menu}>
                            <MenuBar aboutGames={categories} activeGame={activeGame} setActiveGame={setActiveGame}/>
                        </Sider>
                        <Layout className={gridStyles.gridStyles__content}>
                            <Content>
                                <ContentGame fetchGameFaq={fetchGameFaq}
                                             fetchCategoriesFaq={fetchCategoriesFaq}
                                             fetchOffersFaq={fetchOffersFaq}
                                             title={categories.find(category => category.id === activeGame)} />
                            </Content>
                        </Layout>
                    </Layout>
                </main>
            </div>
        )
    }

}


export default Home
