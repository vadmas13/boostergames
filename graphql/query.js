import gql from 'graphql-tag'

export const FETCH_CATEGORIES_PARENTS_QUERY = gql`
    query allCategories{
        allCategories{
            edges{
                node{
                    id
                    name
                    parent{
                        id
                        name
                    }
                }
            }
        }
    }
`

export const FETCH_FAQ_OF_GAME = gql`
    query category($id: String){
        category(id: $id){
            id
            faq{
                edges{
                    node{
                        id
                        question
                        answer
                    }
                }
            }
        }
    }
`

export const FETCH_FAQ_OF_CATEGORIES = gql`
    query category($id: String){
        category(id: $id){
            id
            children{
                edges{
                    node{
                        id
                        name
                        faq{
                            edges{
                                node{
                                    id
                                    question
                                    answer
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`

export const FETCH_FAQ_OFFERS = gql`    
    query category($id: String){
        category(id: $id){
            id
            offers{
              edges{
                node{
                  faq{
                    edges{
                      node{
                        question
                        answer
                      }
                    }
                  }
                }
              }
            }
        }
    }
`
