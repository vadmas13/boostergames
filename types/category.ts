interface DataFaqTypes {
    data: any | CategoryTypes,
    loading: boolean/*,
    called: boolean,
    client: any,
    error: any,
    fetchMore: any,
    networkStatus: number*/
}

interface CategoryTypes {
    category: {
        id: string,
        faq?: {
            edges: Array<EdgesTypeCategory>
        },
        children?: {
            edges: Array<ChildrenEdgesType>
        },
        offers: {
            edges: Array<ChildrenEdgesType>
        }
    }
}

interface ChildrenEdgesType {
    node: {
        id: string,
        name: string,
        faq: {
            edges: Array<EdgesTypeCategory>
        }
    }
}

interface EdgesTypeCategory {
    node: {
        id?: string,
        question: string,
        answer: string
    }
}

export default DataFaqTypes





