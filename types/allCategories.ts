interface allCategoriesType {
    readonly allCategories: EdgesType
}

export interface EdgesType {
    edges: Array<NodeType>
}

export interface NodeType {
    node: {
        id: string,
        name: string,
        parent: ParentType | null
    }
}

export interface ParentType {
    id: string,
    name: string
}

export default allCategoriesType;