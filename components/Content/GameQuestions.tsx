import React, {FC} from "react";
import {CollapseFaq, PanelFaq} from "../../util/hooks/collapse";
import Preloader from "../Preloader";
import EmptyFaq from "./EmptyFaq";
import DataFaqTypes from './../../types/category'


type fetchGameFaqType = {
    fetchGameFaq: DataFaqTypes
}


const GameQuestions: FC<fetchGameFaqType> = ({fetchGameFaq}) => {


    if (fetchGameFaq.loading) {
        return <Preloader/>
    } else return (
        <>
            {   fetchGameFaq.data && fetchGameFaq.data.category.faq.edges.length ?
                (<CollapseFaq>
                    {   fetchGameFaq.data.category.faq.edges.map((item, i) => (

                        <PanelFaq header={item.node.question} key={`${i}_${item.node.question}`}>
                            <p> {item.node.answer} </p>
                        </PanelFaq>

                    ))}
                </CollapseFaq>) : (
                    <EmptyFaq/>
                )
            }

        </>
    )

}

export default GameQuestions