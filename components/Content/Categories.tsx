import React from "react";
import { FC } from "react";
import {CollapseFaq, PanelFaq} from "../../util/hooks/collapse";
import Preloader from "../Preloader";
import EmptyFaq from "./EmptyFaq";
import DataFaqTypes from "../../types/category";



type CategoriesType = {
    fetchCategoriesFaq: DataFaqTypes
}

const Categories: FC<CategoriesType> = ({fetchCategoriesFaq}) => {


    if(fetchCategoriesFaq.loading){
        return <Preloader />
    }else return (
        <>
            {fetchCategoriesFaq.data && fetchCategoriesFaq.data.category.children.edges.length ?
                (
                    <CollapseFaq>
                    {fetchCategoriesFaq.data.category.children.edges.map((item, i) => (

                        <PanelFaq header={item.node.name} key={`${i}_${item.node.question}`}
                                  extra={item.node.faq.edges.length ? item.node.faq.edges.length : ''}>
                            {item.node.faq.edges.length ? (
                                <CollapseFaq accordion>
                                    {item.node.faq.edges.map((faqItem, i) => (
                                        <PanelFaq header={faqItem.node.question}
                                                  key={`${faqItem.node.question}_${i}`}>
                                            {faqItem.node.answer}
                                        </PanelFaq>
                                    ))}
                                </CollapseFaq>
                            ) :  <EmptyFaq />}
                        </PanelFaq>

                    ))}
                    </CollapseFaq>)
                : (
                    <EmptyFaq />
                )
            }
        </>
    )
}

export default Categories