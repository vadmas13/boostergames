import React from "react";
import { FC } from "react";
import {CollapseFaq, PanelFaq} from "../../util/hooks/collapse";
import Preloader from "../Preloader";
import EmptyFaq from "./EmptyFaq";
import DataFaqTypes from "../../types/category";



type fetchOffersFaqType = {
    fetchOffersFaq: DataFaqTypes
}


const OffersQuestions: FC<fetchOffersFaqType> = ({fetchOffersFaq}) => {

    if(fetchOffersFaq.loading){
        return <Preloader />
    }else return (
        <>
            {
                fetchOffersFaq.data && fetchOffersFaq.data.category.offers.edges.length ?
                    fetchOffersFaq.data.category.offers.edges.map((item, i) => {
                            if (item.node.faq.edges.length){
                                return (
                                    <CollapseFaq key={`${i}_offers`}>
                                        {
                                            item.node.faq.edges.map((faqItem, i) => (
                                                <PanelFaq header={faqItem.node.question}
                                                          key={`${faqItem.node.question}_${i}`}>
                                                    {faqItem.node.answer}
                                                </PanelFaq>
                                            ))
                                        }
                                    </CollapseFaq>
                                )
                            } else {
                                return (
                                    <EmptyFaq />
                                )
                            }
                        }
                    ) : (
                        <EmptyFaq />
                    )
            }

        </>
    )
}

export default OffersQuestions