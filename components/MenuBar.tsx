import React, { FC } from 'react';
import {Menu, Spin} from 'antd';
import {LoadingOutlined} from '@ant-design/icons';
import styles from './../assets/styles/MenuBar.module.scss'
import {ParentType} from "../types/allCategories";


const {SubMenu} = Menu;
const antIcon = <LoadingOutlined style={{fontSize: 24}} spin/>;

type MenuBarType = {
    aboutGames: Array<ParentType>,
    activeGame: string,
    setActiveGame(name: string): Array<ParentType>
}


const MenuBar: FC<MenuBarType> = ({aboutGames, activeGame, setActiveGame}) => {

    const handleClick = (e) => {
        setActiveGame(e.key.toString());
    }

    return (
                <Menu
                    onClick={(e) => handleClick(e)}
                    className={styles.MenuBar}
                    defaultOpenKeys={['sub2']}
                    selectedKeys={[activeGame]}
                    mode="inline"
                >
                    <SubMenu key="sub1" title="General">

                    </SubMenu>
                    <SubMenu key="sub2" title="About games">
                        {aboutGames ? aboutGames.map(item => (
                            <Menu.Item key={item.id}>{item.name}</Menu.Item>
                        )) : <Spin indicator={antIcon} className={styles.MenuBar__spin}/>}

                    </SubMenu>
                    <SubMenu key="sub3" title="For Boosters">

                    </SubMenu>
                </Menu>
    );
}

export default MenuBar