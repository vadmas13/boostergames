import React, { FC } from 'react'
import contentStyles from './../assets/styles/ContentGame.module.scss'
import GameQuestions from "./Content/GameQuestions";
import Categories from "./Content/Categories";
import OffersQuestions from "./Content/OffersQuestions";
import DataFaqTypes from './../types/category'
import {ParentType} from "../types/allCategories";


type ContentGameTypes = {
    title: ParentType | undefined,
    fetchGameFaq: DataFaqTypes,
    fetchCategoriesFaq: DataFaqTypes,
    fetchOffersFaq: DataFaqTypes
}

const ContentGame: FC<ContentGameTypes> = (props) => {

    const {title, fetchGameFaq, fetchCategoriesFaq, fetchOffersFaq} = props;

    return (
        <div className={contentStyles.ContentGame}>
            <h1 className={contentStyles.ContentGame__title}>About game {title && title.name}</h1>
            <GameQuestions fetchGameFaq={fetchGameFaq} />
            <h1 className={`${contentStyles.ContentGame__title} ${contentStyles.ContentGame__title_offset}`}>Categories {title && title.name}</h1>
            <Categories fetchCategoriesFaq={fetchCategoriesFaq} />
            <h1 className={`${contentStyles.ContentGame__title} ${contentStyles.ContentGame__title_offset}`}>Offers {title && title.name}</h1>
            <OffersQuestions fetchOffersFaq={fetchOffersFaq}/>
        </div>
    )
}

export default ContentGame