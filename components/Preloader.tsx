import {Spin} from 'antd';
import {LoadingOutlined} from '@ant-design/icons';
import stylesContent from './../assets/styles/ContentGame.module.scss'

const antIcon = <LoadingOutlined style={{fontSize: 30, color: '#ffffff'}} spin/>;


export default () => {
    return (
        <div className={stylesContent.preloader}>
            <Spin indicator={antIcon}/>
        </div>
    )
}