import React from 'react'
import {Spin} from "antd";


const LoadingPage = () => {

    return (
        <div style={{
            display: "flex",
            flexFlow: "column",
            justifyContent: "center",
            alignItems: "center",
            height: "100%",
            textAlign: "center",
            margin: "100px auto" }}>
            <h1>It's very long loading because MenuItems are in the parent field</h1>
            <Spin size="large"/>
        </div>
    )
}
export default LoadingPage